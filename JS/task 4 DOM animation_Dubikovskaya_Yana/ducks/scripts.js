let example = document.getElementById("ducks"),
    ctx = example.getContext("2d");
ctx.fillStyle = "#E3F4FB";
ctx.fillRect(0, 0, example.width, example.height);

function getRadians(degrees) {
    return (Math.PI / 180) * degrees;
}

//bulrush
//1
ctx.lineCap = "round";
ctx.lineWidth = 7;
ctx.beginPath();
ctx.moveTo(25, 70);
ctx.quadraticCurveTo(45, 100, 50, 150);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(30, 40);
ctx.quadraticCurveTo(52, 90, 50, 150);
ctx.moveTo(70, 80);
ctx.quadraticCurveTo(45, 120, 52, 150);
ctx.strokeStyle = "#1DB646";
ctx.stroke();

//2
ctx.beginPath();
ctx.lineWidth = 4;
ctx.moveTo(128, 90);
ctx.quadraticCurveTo(102, 115, 108, 150);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(120, 70);
ctx.quadraticCurveTo(100, 120, 105, 150);
ctx.moveTo(90, 115);
ctx.quadraticCurveTo(110, 130, 102, 150);
ctx.strokeStyle = "#1DB646";
ctx.stroke();

//3
ctx.beginPath();
ctx.lineWidth = 7;
ctx.lineJoin = 'bevel';
ctx.moveTo(130, 80);
ctx.lineTo(140, 55);
ctx.lineTo(160, 135);
ctx.moveTo(190, 50);
ctx.quadraticCurveTo(155, 95, 163, 135);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.lineWidth = 3;
ctx.moveTo(158, 135);
ctx.lineTo(158, 45);
ctx.moveTo(158, 25);
ctx.lineTo(158, 13);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.lineWidth = 8;
ctx.moveTo(158, 45);
ctx.lineTo(158, 20);
ctx.strokeStyle = "#BC7D57";
ctx.stroke();

//4
ctx.beginPath();
ctx.moveTo(280, 30);
ctx.quadraticCurveTo(255, 50, 248, 90);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(220, 60);
ctx.quadraticCurveTo(240, 70, 245, 90);
ctx.moveTo(270, 10);
ctx.quadraticCurveTo(245, 30, 245, 90);
ctx.strokeStyle = "#1DB646";
ctx.stroke();

//5
ctx.beginPath();
ctx.moveTo(320, 30);
ctx.quadraticCurveTo(340, 50, 350, 90);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(330, 10);
ctx.quadraticCurveTo(355, 30, 350, 90);
ctx.moveTo(380, 60);
ctx.quadraticCurveTo(360, 70, 350, 90);
ctx.moveTo(320, 80);
ctx.quadraticCurveTo(345, 100, 350, 160);
ctx.strokeStyle = "#1DB646";
ctx.stroke();

//6
ctx.beginPath();
ctx.lineWidth = 6;
ctx.moveTo(340, 340);
ctx.quadraticCurveTo(360, 360, 370, 400);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(350, 320);
ctx.quadraticCurveTo(375, 340, 370, 400);
ctx.moveTo(400, 370);
ctx.quadraticCurveTo(380, 360, 370, 400);
ctx.strokeStyle = "#1DB646";
ctx.stroke();

//7
ctx.beginPath();
ctx.lineWidth = 7;
ctx.lineJoin = 'bevel';
ctx.moveTo(380, 340);
ctx.lineTo(390, 315);
ctx.lineTo(410, 395);
ctx.moveTo(440, 310);
ctx.quadraticCurveTo(405, 355, 413, 395);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.lineWidth = 3;
ctx.moveTo(408, 395);
ctx.lineTo(408, 285);
ctx.moveTo(408, 250);
ctx.lineTo(408, 245);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.lineWidth = 8;
ctx.moveTo(408, 285);
ctx.lineTo(408, 250);
ctx.strokeStyle = "#BC7D57";
ctx.stroke();

//9
ctx.beginPath();
ctx.lineWidth = 7;
ctx.lineJoin = 'bevel';
ctx.moveTo(430, 320);
ctx.lineTo(440, 295);
ctx.lineTo(460, 375);
ctx.moveTo(500, 290);
ctx.quadraticCurveTo(455, 355, 463, 375);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.lineWidth = 3;
ctx.moveTo(460, 375);
ctx.lineTo(460, 250);
ctx.moveTo(460, 230);
ctx.lineTo(460, 220);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.lineWidth = 8;
ctx.moveTo(460, 250);
ctx.lineTo(460, 230);
ctx.strokeStyle = "#BC7D57";
ctx.stroke();

//8
ctx.beginPath();
ctx.lineWidth = 5;
ctx.moveTo(415, 355);
ctx.quadraticCurveTo(440, 350, 440, 395);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(455, 335);
ctx.quadraticCurveTo(440, 360, 445, 395);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(450, 325);
ctx.quadraticCurveTo(435, 350, 440, 385);
ctx.strokeStyle = "#1DB646";
ctx.stroke();

//10
ctx.beginPath();
ctx.lineWidth = 6;
ctx.moveTo(475, 355);
ctx.quadraticCurveTo(505, 350, 505, 395);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(535, 335);
ctx.quadraticCurveTo(510, 360, 510, 395);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(530, 325);
ctx.quadraticCurveTo(500, 350, 510, 395);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
//11
ctx.beginPath();
ctx.lineWidth = 4;
ctx.moveTo(580, 340);
ctx.quadraticCurveTo(600, 360, 610, 400);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(590, 320);
ctx.quadraticCurveTo(615, 340, 610, 400);
ctx.moveTo(640, 370);
ctx.quadraticCurveTo(620, 360, 610, 400);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
//12
ctx.beginPath();
ctx.lineWidth = 4;
ctx.moveTo(610, 340);
ctx.quadraticCurveTo(630, 360, 640, 400);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(620, 320);
ctx.quadraticCurveTo(645, 340, 640, 400);
ctx.moveTo(660, 370);
ctx.quadraticCurveTo(645, 360, 643, 400);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
//13
ctx.beginPath();
ctx.moveTo(702, 335);
ctx.quadraticCurveTo(682, 350, 684, 400);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(665, 370);
ctx.quadraticCurveTo(678, 380, 680, 400);
ctx.moveTo(648, 320);
ctx.quadraticCurveTo(683, 340, 683, 400);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
//14
ctx.beginPath();
ctx.moveTo(584, 60);
ctx.quadraticCurveTo(564, 75, 566, 120);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(540, 80);
ctx.quadraticCurveTo(555, 93, 563, 120);
ctx.moveTo(538, 30);
ctx.quadraticCurveTo(565, 50, 565, 120);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
//15
ctx.beginPath();
ctx.lineWidth = 7;
ctx.moveTo(670, 100);
ctx.quadraticCurveTo(690, 120, 700, 160);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(680, 80);
ctx.quadraticCurveTo(705, 100, 700, 160);
ctx.moveTo(725, 130);
ctx.quadraticCurveTo(705, 135, 700, 160);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
//16
ctx.beginPath();
ctx.lineWidth = 4;
ctx.moveTo(700, 90);
ctx.quadraticCurveTo(715, 100, 720, 120);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(700, 70);
ctx.quadraticCurveTo(725, 90, 720, 120);
ctx.moveTo(740, 110);
ctx.quadraticCurveTo(730, 112, 722, 120);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
//17
ctx.beginPath();
ctx.moveTo(770, 120);
ctx.quadraticCurveTo(765, 135, 762, 160);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.lineWidth = 5;
ctx.moveTo(740, 130);
ctx.quadraticCurveTo(755, 135, 760, 160);
ctx.moveTo(760, 100);
ctx.quadraticCurveTo(755, 125, 760, 160);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
//18
ctx.beginPath();
ctx.lineWidth = 6;
ctx.lineJoin = 'bevel';
ctx.moveTo(780, 140);
ctx.lineTo(790, 115);
ctx.lineTo(805, 195);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.lineWidth = 3;
ctx.moveTo(808, 195);
ctx.lineTo(808, 105);
ctx.moveTo(808, 80);
ctx.lineTo(808, 72);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.lineWidth = 8;
ctx.moveTo(808, 105);
ctx.lineTo(808, 80);
ctx.strokeStyle = "#BC7D57";
ctx.stroke();
//19
ctx.beginPath();
ctx.lineWidth = 6;
ctx.lineJoin = 'bevel';
ctx.moveTo(860, 120);
ctx.lineTo(850, 100);
ctx.lineTo(840, 175);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.lineWidth = 3;
ctx.moveTo(838, 175);
ctx.lineTo(838, 90);
ctx.moveTo(838, 60);
ctx.lineTo(838, 52);
ctx.strokeStyle = "#1DB646";
ctx.stroke();
ctx.beginPath();
ctx.lineWidth = 8;
ctx.moveTo(838, 90);
ctx.lineTo(838, 60);
ctx.strokeStyle = "#BC7D57";
ctx.stroke();
//20
ctx.beginPath();
ctx.lineWidth = 7;
ctx.moveTo(810, 165);
ctx.quadraticCurveTo(835, 180, 840, 220);
ctx.strokeStyle = "#B2E721";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(815, 135);
ctx.quadraticCurveTo(840, 155, 840, 220);
ctx.moveTo(855, 170);
ctx.quadraticCurveTo(840, 210, 840, 220);
ctx.strokeStyle = "#1DB646";
ctx.stroke();

//WAVES
//1
ctx.beginPath();
ctx.lineWidth = 2;
ctx.moveTo(15, 170);
ctx.bezierCurveTo(30, 150, 50, 190, 70, 160);
ctx.strokeStyle = "#ABCFDA";
//2
ctx.moveTo(140, 150);
ctx.bezierCurveTo(150, 130, 160, 170, 180, 140);
//3
ctx.moveTo(70, 320);
ctx.bezierCurveTo(80, 300, 90, 340, 110, 310);
//4
ctx.moveTo(110, 340);
ctx.bezierCurveTo(120, 320, 130, 360, 150, 330);
//5
ctx.moveTo(190, 345);
ctx.bezierCurveTo(200, 335, 210, 355, 230, 340);
//6
ctx.moveTo(270, 280);
ctx.bezierCurveTo(280, 270, 290, 290, 310, 275);
//7
ctx.moveTo(280, 360);
ctx.bezierCurveTo(290, 340, 300, 380, 320, 360);
//8
ctx.moveTo(360, 150);
ctx.bezierCurveTo(370, 130, 380, 170, 400, 150);
//9
ctx.moveTo(480, 250);
ctx.bezierCurveTo(490, 230, 500, 270, 520, 250);
//10
ctx.moveTo(520, 220);
ctx.bezierCurveTo(530, 200, 540, 240, 560, 220);
//11
ctx.moveTo(535, 300);
ctx.bezierCurveTo(545, 280, 555, 310, 575, 295);
//12
ctx.moveTo(615, 95);
ctx.bezierCurveTo(635, 105, 640, 92, 655, 98);
//13
ctx.moveTo(800, 210);
ctx.lineTo(815, 210);
//14
ctx.moveTo(850, 240);
ctx.lineTo(863, 243);
//15
ctx.moveTo(590, 180);
ctx.lineTo(603, 180);
//16
ctx.moveTo(820, 360);
ctx.bezierCurveTo(840, 370, 845, 357, 860, 363);
//17
ctx.moveTo(790, 350);
ctx.bezierCurveTo(810, 360, 815, 347, 830, 353);
//18
ctx.moveTo(720, 350);
ctx.bezierCurveTo(740, 360, 745, 347, 740, 353);
ctx.stroke();

//DUCKS
//1
//head
ctx.beginPath();
ctx.lineWidth = 4;
ctx.arc(260, 200, 35, 0, getRadians(360));
ctx.strokeStyle = "#010101";
ctx.fillStyle = "#FFF203";
ctx.stroke();
ctx.fill();
//beak of a beard
ctx.beginPath();
ctx.lineWidth = 2;
ctx.ellipse(300, 205, 5, 8, getRadians(-90), getRadians(45), getRadians(335));
ctx.moveTo(297, 200);
ctx.lineTo(297, 194);
ctx.lineTo(304, 201);
ctx.fillStyle = "#FA7F24";
ctx.stroke();
ctx.fill();
//tail of bird
ctx.beginPath();
ctx.lineWidth = 3;
ctx.moveTo(100, 230);
ctx.lineTo(145, 255);
ctx.lineTo(114, 255);
ctx.closePath();
ctx.fillStyle = "#FFF203";
ctx.stroke();
ctx.fill();
//body
ctx.beginPath();
ctx.ellipse(190, 265, 45, 75, getRadians(90), 0, getRadians(360));
ctx.fillStyle = "#FFF203";
ctx.stroke();
ctx.fill();
//wings of duck
ctx.beginPath();
ctx.lineWidth = 2;
ctx.ellipse(195, 265, 20, 35, getRadians(-90), getRadians(30), getRadians(250));
ctx.stroke();
//eye of duck
ctx.beginPath();
ctx.arc(275, 195, 7, 0, getRadians(360));
ctx.stroke();
ctx.fillStyle = "#010101";
ctx.fill();

//2
//head
ctx.beginPath();
ctx.lineWidth = 4;
ctx.arc(450, 100, 35, 0, getRadians(360));
ctx.strokeStyle = "#010101";
ctx.fillStyle = "#FFF203";
ctx.stroke();
ctx.fill();
//beak of a beard
ctx.beginPath();
ctx.lineWidth = 2;
ctx.moveTo(405, 90);
ctx.lineTo(415, 85);
ctx.lineTo(416, 93);
ctx.ellipse(410, 95, 5, 8, getRadians(-90), getRadians(45), getRadians(335));
ctx.fillStyle = "#FA7F24";
ctx.stroke();
ctx.fill();
//eye of duck
ctx.beginPath();
ctx.arc(435, 90, 7, 0, getRadians(360));
ctx.stroke();
ctx.fillStyle = "#010101";
ctx.fill();
//tail of bird
ctx.beginPath();
ctx.lineWidth = 3;
ctx.moveTo(585, 140);
ctx.lineTo(540, 165);
ctx.lineTo(570, 165);
ctx.closePath();
ctx.fillStyle = "#FFF203";
ctx.stroke();
ctx.fill();
//body
ctx.beginPath();
ctx.ellipse(495, 175, 45, 75, getRadians(90), 0, getRadians(360));
ctx.fillStyle = "#FFF203";
ctx.stroke();
ctx.fill();
//wings of duck
ctx.beginPath();
ctx.lineWidth = 2;
ctx.ellipse(495, 175, 20, 35, getRadians(-90), getRadians(100), getRadians(320));
ctx.stroke();

//3
//head
ctx.beginPath();
ctx.lineWidth = 4;
ctx.arc(710, 210, 35, 0, getRadians(360));
ctx.strokeStyle = "#010101";
ctx.fillStyle = "#FFF203";
ctx.stroke();
ctx.fill();
//beak of a beard
ctx.beginPath();
ctx.lineWidth = 2;
ctx.moveTo(671, 200);
ctx.lineTo(681, 195);
ctx.lineTo(679, 203);
ctx.ellipse(675, 205, 5, 8, getRadians(-90), getRadians(45), getRadians(335));
ctx.fillStyle = "#FA7F24";
ctx.stroke();
ctx.fill();
//eye of duck
ctx.beginPath();
ctx.arc(698, 197, 7, 0, getRadians(360));
ctx.stroke();
ctx.fillStyle = "#010101";
ctx.fill();
//tail of bird
ctx.beginPath();
ctx.lineWidth = 3;
ctx.moveTo(847, 255);
ctx.lineTo(812, 280);
ctx.lineTo(832, 280);
ctx.closePath();
ctx.fillStyle = "#FFF203";
ctx.stroke();
ctx.fill();
//body
ctx.beginPath();
ctx.ellipse(760, 284, 45, 75, getRadians(90), 0, getRadians(360));
ctx.fillStyle = "#FFF203";
ctx.stroke();
ctx.fill();
//wings of duck
ctx.beginPath();
ctx.lineWidth = 2;
ctx.ellipse(760, 280, 20, 35, getRadians(-90), getRadians(100), getRadians(320));
ctx.stroke();


