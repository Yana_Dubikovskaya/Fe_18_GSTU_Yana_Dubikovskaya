let frog = new Image(256, 256);
frog.style.backgroundImage = "url('img/game1.png')";
frog.className = "frog-with-cloth-mouth";
for (let i = 0; i < 7; i++) {
    let mosquito = new Image(45, 45);
    mosquito.src = "img/moscito.png";
    mosquito.className = "mosquito";
    mosquito.style.top = (Math.random() * 300 + 100) + "px";
    mosquito.style.left = (Math.random() * 365 + 470) + "px";
    mosquito.setAttribute("onmousedown", "Move(this);");
    document.body.appendChild(mosquito);


    function Eating(obj) {
        var frog = document.querySelector('.frog-with-cloth-mouth');
        if (obj.x < frog.x + frog.offsetWidth && obj.x > frog.x) {
            frog.style.transition = 'ease 0.5s';
            frog.style.transformOrigin = '0 50%';
            frog.style.backgroundImage = "url('img/game3.png')";
            obj.onmouseup = function () {
                Remove(obj);
            };
        }
        else {
            frog.style.backgroundImage = "url('img/game1.png')";

            obj.onmouseup = function () {
                document.onmousemove = null;
                document.onmouseup = null;
            };
        }
    }

    function Remove(obj) {
        obj.className = ' removed ';
        setTimeout(function () {
            obj.parentElement.removeChild(obj);
        }, 2000);
    }

    function Move(obj) {
        document.onmousemove = function (e) {
            moveAt(e);
            Eating(obj);
        };
        obj.onmouseup = function () {
            document.onmousemove = null;
            document.onmouseup = null;
        };
        var shiftX, shiftY;
        this.onmousedown = function (e) {
            shiftY = e.pageY - obj.y;
            shiftX = e.pageX - obj.x;
        };

        function moveAt(e) {
            obj.style.left = e.pageX - shiftX + 'px';
            obj.style.top = e.pageY - shiftY + 'px';
            obj.style.zIndex = 100;
        }

        obj.ondragstart = function (e) {
            return false;
        };
    }
    document.body.appendChild(frog);
}