/*Task 1*/
console.log("Task 1");
var a = 12,
    b = 13;
console.log("Result: " + conc(a, b));

/*
 * function, which concatenate two parametrs
 */
function conc(paramOne, paramTwo) {
    return "" + paramOne + paramTwo;
}
/*End Task 1*/

/*Task 2*/
console.log("Task 2");
/*
 * function, which compare strings
 */
var comp = function(paramOne, paramTwo) {
    if (paramOne === paramTwo) {
        return 1;
    } else {
        return -1;
    }
}

var a = "Abc",
    b = "Abc";
console.log(comp(a, b));
/*End Task 2*/

/*Task 3*/
console.log("Task 3");
document.getElementById('btn').onclick = function () {
    console.log("message in console");
}
/*End Task 3*/

/*Task 4*/
console.log("Task 4");
/*
 * calcuate fibonachi numbers
 */
function fibo(number) {
    let curr = 1,
        prev = 0;
    for (let i = 1; i < number; i++) {
        curr += prev;
        prev = curr - prev;
    }
    return curr;
}

console.log(fibo(10));
/*End Task 4*/

/*Task 5*/
console.log("Task 5");
/*
 * concatenate two paraments
 */
console.log((function conc(paramOne, paramTwo) {
    return ("" + paramOne + paramTwo);
}(2, 3)));
/*End Task 5*/

/*Task 6*/
console.log("Task 6");
/*
 * extract the substring from the sign “:”(colon) to the sign “.”(period)
 */
var extractTheSubstring = function() {
    let arrayOfSubstrings = [];
    for (let i = 0; i < arguments.length; i++) {
        let matches = arguments[i].match(/\:[A-Za-z0-9 _,!"'/]*\./g);
        for (let j = 0; j < matches.length; j++) {
            arrayOfSubstrings.push(matches[j].slice(1, -1));
        }
    }
    console.log(arrayOfSubstrings);
}
var textOne = "This is the first sentence. This is a sentence with a list of items: cherries, oranges, apples, bananas.";
textTwo = "This is the second sentence. This is a sentence with a list of items: red, blue, yellow, black.";
extractTheSubstring(textOne, textTwo);
/*End Task 6*/

/*Task 7*/
console.log("Task 7");
/*
 *find the position of test string in testString
 */
var find = function(testString, test) {
    test = test || testString;
    return testString.indexOf(test);
}

console.log(find("abc", "b"));
console.log(find("abc"));
console.log(find("abc", "d"));
console.log(find("abc", "a", "b"));
/*End Task 7*/

/*Task 8*/
console.log("Task 8");

/*
 *print result of checking parameters on emptyness
 */
var str = function(parameter) {
    if (str.isNonEmptyStr(parameter)) {
        alert("String is non empty");
    } else {
        alert("String is empty");
    }
}
/*
 *checking parameters on emptyness
 */
str.isNonEmptyStr = function(param) {
    if (param !== "" && param !== undefined &&
        (typeof param === 'string' || param instanceof String)) {
        return true;
    } else {
        return false;
    }
}

console.log(str.isNonEmptyStr());
console.log(str.isNonEmptyStr(""));
console.log(str.isNonEmptyStr("a"));
console.log(str.isNonEmptyStr(1));
str();
str("a");
/*End Task 8*/

/*Task 9*/
console.log("Task 9");
/*
 * print result to console
 */
function toConsole(parameter) {
    console.log(parameter);
}
/*
 * print result to alert
 */
function toAlert(parameter) {
    alert(parameter);
}
/*
 *spliting messafe into words
 */
function splitToWords(msg, callback) {
    let words = msg.split(" ");
    if (callback === undefined) {
        return words;
    }
    for (let i = 0; i < words.length; i++) {
        callback(words[i]);
    }

}
splitToWords("My very long text msg", toConsole);
splitToWords("My very long text msg", toAlert);
console.log(splitToWords("My very long text msg"));
/*End Task 9*/

/*Task 10*/
console.log("Task 10");
/*
 *the function add the copyright sign
 */
function createCopyright() {
    let sign = "\u00A9";
    return (function addSign(parameter) {
        return sign + parameter;
    });
}
console.log(createCopyright()("EPAM"));
/*End Task 10*/

/*Task 11*/
console.log("Task 11");
var employee = {
    name: "Anna",
    work: function() {
        console.log("I am " + this.name + ". I am working")
    }
};
employee.work();
/*End Task 11*/