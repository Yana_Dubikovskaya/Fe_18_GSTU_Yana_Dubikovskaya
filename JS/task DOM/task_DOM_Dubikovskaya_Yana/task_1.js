console.log("Task 1");

//get element with method querySelectorAll
function getElementsWithQuerySelectorAll(element) {
    return document.querySelectorAll(element);
}

//get element with method getElementsByTagName
function getElementsWithSpecifiedTag(element) {
    let elementByTagName = document.getElementsByTagName(element);
    let arrayElem = [];
    for (let i = 0; i < elementByTagName.length; i++) {
        arrayElem.push(elementByTagName[i]);
    }
    return arrayElem;
}

console.log("Get element with method querySelectorAll: ", getElementsWithQuerySelectorAll('#first_block div'));
console.log("Get element with method getElementsByTagName: ", getElementsWithSpecifiedTag('input'));

console.log("");
console.log("task 2");
// function that gets the value of <input type="text">
// and prints its value to the console
function getValue(tagName) {
    return document.querySelector(tagName).value;
}

console.log("The value obtained: ", getValue('input'));

console.log("");
console.log("task 3");
window.onload = init;

function init() {
    document.getElementById('input_color').onclick;
}

/**
 * function that gets the value of <input type="color">
 * and sets the background of the body to this value
 */
function setColorOnTheBackground(tagName) {
    document.body.style.backgroundColor = document.getElementById(tagName).value;
}