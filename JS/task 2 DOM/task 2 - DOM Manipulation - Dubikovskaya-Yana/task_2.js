console.log(" ");
console.log("Task 2. DOM manipulation");

/* Task 1 */
console.log("Task 1");

//create random values of height & width
function getRandomWSize() {
    return (Math.floor(Math.random() * 81) + 20).toString();
}

//create random color
function getRandomColor() {
    return '#' + (Math.random().toString(16) + "000000").substring(2, 8);
}

//create random borderRadius, top, top
function getRandomValue() {
    return (Math.floor(Math.random() * 71) + 1).toString();
}

function getRandomBorderWidth() {
    return (Math.floor(Math.random() * 21) + 1).toString();
}

//create div-element
function createElement(amountOfElements) {
    for (let i = 0; i < amountOfElements; i++) {
        let randomWidth = getRandomWSize();
        let randomWHeight = getRandomWSize();
        let color = getRandomColor();
        let colorBorder = getRandomColor();
        let colorTitle = getRandomColor();
        let borderRadius = getRandomValue();
        let borderWidth = getRandomBorderWidth();
        let top = getRandomValue();
        let right = getRandomValue();
        let rootDiv = document.createElement('div');
        rootDiv.style.width = randomWidth + "px";
        rootDiv.style.height = randomWHeight + "px";
        rootDiv.style.backgroundColor = color;
        rootDiv.style.color = color;
        rootDiv.style.position = "absolute";
        rootDiv.style.top = top + "px";
        rootDiv.style.right = right + "px";
        rootDiv.style.border = "solid";
        rootDiv.style.borderRadius = borderRadius + "px";
        rootDiv.style.borderColor = colorBorder;
        rootDiv.style.borderWidth = borderWidth + "px";
        rootDiv.style.color = colorTitle;
        let strong = document.createElement('strong');
        strong.innerHTML = "div";
        rootDiv.appendChild(strong);
        document.getElementById("div-elements").appendChild(rootDiv);
    }
}

createElement(8);
/* End Task 1 */

/* Task 2 */
console.log("");
console.log("Task 2");

/**
 *  function creates 5 "div" elements and moves them in circular path
 *  with interval of 100 milliseconds
 */
function moveDivElements() {
    let field = document.getElementById("field_moving");
    for (let i = 0; i < 5; i++) {
        let div = document.createElement('div');
        div.style.width = "20px";
        div.style.height = "20px";
        div.style.left = "0";
        div.style.position = "absolute";
        div.style.backgroundColor = "red";
        document.getElementById("field_moving").appendChild(div);
        let maxX = field.clientWidth - div.offsetWidth;
        let maxY = field.clientHeight - div.offsetHeight;
        let duration = 8; // seconds
        let gridSize = 100; // pixels
        let start = null;
        let stretchFactor;

        function step(timestamp) {
            setTimeout(function () {
                let progress, x, y;
                if (start === null) {
                    start = timestamp;
                    stretchFactor = 1 + (Math.random() * 2);
                }
                progress = (timestamp - start) / duration / 1000; // percent

                x = stretchFactor * Math.sin(progress * 2 * Math.PI); // x = ƒ(t)
                y = Math.cos(progress * 2 * Math.PI); // y = ƒ(t)

                div.style.left = maxX / 2 + (gridSize * x) + "px";
                div.style.bottom = maxY / 2 + (gridSize * y) + "px";

                if (progress >= 1) start = null; // reset to start position
                requestAnimationFrame(step);
            }, 100);
        }

        requestAnimationFrame(step);
    }
}

moveDivElements();
/*End Task 2*/

/* Task 3 */
/**
 * function, which create textarea and inputs
 */
function createTextareaAndInputs() {
    let textarea = document.createElement("TEXTAREA");
    let text = document.createTextNode("color");
    textarea.appendChild(text);
    document.getElementById("task3_block").appendChild(textarea);
    for (let i = 0; i < 2; i++) {
        let input = document.createElement("INPUT");
        input.setAttribute("type", "color");
        document.getElementById("task3_block").appendChild(input);
        //Make the font "color" of the text area as the value of the first color "input"
        if (i === 0) {
            input.addEventListener("change", function (event) {
                textarea.style.color = event.target.value;
            });
        }
        //Make the "background-color" of the "textarea" as the value of the second "input"
        else {
            input.addEventListener("change", function (event) {
                textarea.style.backgroundColor = event.target.value;
            });
        }
    }
}

createTextareaAndInputs();
/* End Task 3 */

/* Task 4 */
let tags = [
    "cms", "javascript", "js", "ASP.NET MVC", ".net", ".net", "css", "wordpress", "xaml", "js", "http",
    "web", "asp.net", "asp.net MVC", "ASP.NET MVC", "wp", "javascript", "js", "cms", "html", "javascript",
    "http", "http", "CMS"
];

/**
 * function generate the tags with different "font-size",
 * depending on the number of occurrences
 */
function generateTagCloud(tags, minFontSize, maxFontSize) {
    let map = new Map();
    for (let i = 0; i < tags.length; i++) {
        tags[i] = tags[i].toLowerCase();
        if (map.has(tags[i])) {
            let value = map.get(tags[i]);
            map.set(tags[i], ++value);
        }
        else {
            map.set(tags[i], 1);
        }
    }
    let k = 0;
    let step = (maxFontSize - minFontSize) / map.size;
    map.forEach(function (item, key, mapObj) {
        k++;
        let fontSize = (maxFontSize - minFontSize) / step * item;
        console.log(fontSize);
        let span = document.createElement("span");
        let spanText = document.createTextNode(key + " ");
        span.style.fontSize = fontSize.toString() + "px";
        span.appendChild(spanText);
        if (k === 3) {
            let br = document.createElement("br");
            span.appendChild(br);
            k = 0;
        }
        document.getElementById("task4_block").appendChild(span);
    });
}

console.log(generateTagCloud(tags, 17, 42));
/* End Task 4 */

/* Task 5 */
/**
 * function create nested lists
 */
function createTreeViewComponent(arrayOfLists) {
    for (let i = 0; i < arrayOfLists.length; i++) {
        let ul = document.createElement("ul");
        let li = document.createElement("li");
        let text = document.createTextNode("Item " + (i + 1));
        li.appendChild(text);
        ul.appendChild(li);
        if (arrayOfLists[i].amountOfSubLists !== 0) {
            let sub_ul = document.createElement("ul");
            for (let j = 0; j < arrayOfLists[i].amountOfSubLists; j++) {
                let sub_li = document.createElement("li");
                let sub_text = document.createTextNode("Sub-1 Item " + (j + 1));
                sub_li.appendChild(sub_text);
                sub_ul.appendChild(sub_li);
            }
            li.appendChild(sub_ul);
        }
        document.getElementById("task5_block").appendChild(ul);
        ul.addEventListener("click", function (event) {
            //condition, that checks if List children are visible:
            // if true, they must be made hidden
            // else, they must be made visible
            if (arrayOfLists[i].visible) {
                arrayOfLists[i].visible = false;
                if (!event.target.children[0]) {
                    return;
                }
                for (let child of event.target.children[0].children) {
                    child.style.display = "none";
                }
            }
            else {
                arrayOfLists[i].visible = true;
                if (!event.target.children[0]) {
                    return;
                }
                for (let child of event.target.children[0].children) {
                    child.style.display = "list-item";
                }
            }
        })
    }
}

// Object which represents html List
function CreateList(amountOfSubLists, visible) {
    this.amountOfSubLists = amountOfSubLists;
    this.visible = visible;
}

let item_1 = new CreateList(2, true);
let item_2 = new CreateList(0, true);
let item_3 = new CreateList(3, true);
let item_4 = new CreateList(1, true);

let arrayOfLists = [item_1, item_2, item_3, item_4];
createTreeViewComponent(arrayOfLists);