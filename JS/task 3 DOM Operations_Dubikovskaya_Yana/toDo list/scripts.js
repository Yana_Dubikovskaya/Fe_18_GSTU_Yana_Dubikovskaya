function addNewElement() {
    let li = document.createElement('li');
    let textInput = document.getElementById("input").value;
    if (textInput) {
        li.innerHTML = "<DIV class='text'>" + textInput + "</DIV>" + " <span onclick='deleteElement(this)'>&#9747;</span>" + " <i class=\"fas fa-eye\" onclick=\"showHide(this)\"></i>";
        let ul = document.getElementById('ul');
        ul.appendChild(li);
    }
    else {
        alert("Enter something");
    }
}

function deleteElement(obj) {
    obj.parentNode.remove();
}

function showHide(obj) {
    if (obj.parentNode.firstChild.style.display === "none") {
        obj.style.color = "#FAFAFA";
        obj.parentNode.firstChild.style.display = "block";
    }
    else {
        obj.style.color = "#FFB2B2";
        obj.parentNode.firstChild.style.display = "none";
    }

}