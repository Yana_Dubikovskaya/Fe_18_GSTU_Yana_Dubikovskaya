function searchElement(element, array) {
    //check if Any of the function params is missing
    if (element === undefined || array === undefined) {
        throw Error("Param/params is missing!");
    }
    //check if argument mismatch
    if (typeof element !== "string" && !(element instanceof Element)) {
        throw Error("Argument mismatch!");
    }
    //check if any of the contents is neither "string" nor "number"
    for (let i = 0; i < array.length; i++) {
        if (typeof array[i] !== "string" && typeof  array[i] !== "number") {
            throw Error("Illegal content");
        }
    }
    let currentElement = {};
    if (typeof element === "string") {
        currentElement = document.getElementById(element);
    } else {
        currentElement = element;
    }
    console.log(currentElement);
    removeContent(currentElement);
    if (!currentElement) {
        throw Error("No this element!");
    }
    for (let i = 0; i < array.length; i++) {
        var div = document.createElement('div');
        div.innerHTML = array[i];
        console.log(currentElement.appendChild(div));
    }
}

//function for remove all previous content from the DOM element provided
function removeContent(element) {
    while (element.firstChild) {
        element.firstChild.remove();
    }
}

var currentArray = [1, "Information", 3];
let currentElement = document.getElementById("main-block");
console.log(searchElement(currentElement, currentArray));