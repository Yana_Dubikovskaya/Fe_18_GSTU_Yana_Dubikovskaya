console.log("Task 1");

//constructor Person
function Person(firstname, lastname, age, fullname) {
    this.setFirstName(firstname);
    this.setLastName(lastname);
    this.setAge(age);
    this.setFullName(fullname);
}

/** Create prototype with method checkName, which check condition:
 *  name always be strings between 3 and 20 characters, containing only Latin letters
 **/
Person.prototype.checkName = function (name) {
    let nameRegexp = /^([a-zA-Z-]){3,20}$/;
    let testArray = name.match(nameRegexp);
    console.log(testArray); //что находится в массиве
    if (testArray == null) {
        throw new Error("Conditions aren't met");
    }
};

Person.prototype.setFirstName = function (firstname) {
    this.checkName(firstname);
    this.firstname = firstname;
};
Person.prototype.setLastName = function (lastname) {
    this.checkName(lastname);
    this.lastname = lastname;
};

/**
 * Create prototype with method checkAge,
 * where age must always be a number in the range (0, 150), inclusive
 */
Person.prototype.checkAge = function (age) {
    if (!(age > 0) || !(age <= 150)) {
        throw new Error("Conditions aren't met");
    }
};

/**
 * Create prototype with method setAge,
 * where age can receive a convertible-to-number value
 */
Person.prototype.setAge = function (age) {
    let agePerson = parseInt(age);
    console.log(agePerson);
    this.checkAge(agePerson);
    this.age = agePerson;
};

/**
 * Setter setFullName
 */
Person.prototype.setFullName = function (fullname) {
    this.fullname = fullname;
    let parsedFullName = fullname.split(" ");
    this.firstname = parsedFullName[0];
    this.lastname = parsedFullName[1];
};

/**
 * Getter getFullName
 */
Person.prototype.getFullName = function () {
    return this.fullname.toUpperCase();
};

/**
 * method introduce() that returns a string
 */
Person.prototype.introduce = function () {
    return "Hello! My name is " + this.getFullName() + " and I am " + this.age + " years-old.";
};

let person = new Person("dub", "hkf", "35", "dub hkf");
console.log(person);
console.log(person.introduce());




