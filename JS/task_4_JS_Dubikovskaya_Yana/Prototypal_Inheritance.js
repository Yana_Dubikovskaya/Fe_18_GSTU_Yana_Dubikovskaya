console.log("");
console.log("Task 2");

/**
 * object domElement
 */
var domElement = {
    __domElement__: {},
    contentValue: "",
    attributes: [],
    get parent() {
        return this.__domElement__.parentNode;
    },

    //setter
    set content(value) {
        this.__domElement__.innerHTML = value;
        this.contentValue = value;
    },

    //getter
    get content() {
        return this.contentValue;
    },

    //getter
    get innerHTML() {
        return this.__domElement__;
    },
    //method init() that gets the __domElement__ type
    init: function (__domElement__) {
        this.checkElement(__domElement__);
        this.__domElement__ = document.createElement(__domElement__);
        return this;
    },

    // validation for element
    checkElement: function (element) {
        let nameRegexp = /([a-zA-Z0-9])$/;
        let testArrayOfElement = element.match(nameRegexp);
        if (element === undefined || testArrayOfElement == null) {
            throw new Error("The element failed validation!");
        }
    },

    //  method appendChild
    appendChild: function (child) {
        if (typeof child === "string") {
            this.__domElement__.appendChild(child);
            return this;
        }
        else if (typeof child === "object") {
            this.__domElement__.appendChild(child.innerHTML);
            return this;
        }
        else {
            throw  new Error("The child has incompitable type");
        }
    },

    // method addAttribute checks for validity, adds attributes
    addAttribute: function (name, value) {
        this.attributes = [];
        let nameRegexp = /([a-zA-Z0-9\-])$/;
        let testArrayOfAttribute = name.match(nameRegexp);
        if (name === undefined || testArrayOfAttribute == null) {
            throw new Error("The attribute failed validation!");
        }
        this.attributes.push({name: name, value: value});
        console.log(this.__domElement__);
        for (let i = 0; i < this.attributes.length; i++) {
            let obj = this.attributes[i];
            console.log("obj", obj);
            this.__domElement__.setAttribute(obj.name, obj.value);
        }
        return this;
    },

    // method removeAttribute
    removeAttribute: function (attribute) {
        console.log(this.attributes);
        for (let i = 0; i < this.attributes.length; i++) {
            let obj = this.attributes[i];
            console.log(obj);
            for (let key in obj) {
                if (obj.name === attribute) {
                    this.attributes.splice(i, 1);
                    this.__domElement__.removeAttribute(attribute);
                }
                else {
                    throw new Error("The attribute doesn't exist in domElement");
                }
            }
        }
        return this;
    }
};

var meta = Object.create(domElement)
    .init('meta')
    .addAttribute('charset', 'utf-8');
//.removeAttribute('chet');

var head = Object.create(domElement)
    .init('head')
    .appendChild(meta);

var div = Object.create(domElement)
    .init('div')
    .addAttribute('style', 'font-size: 42px');

div.content = 'Hello, world!';

var body = Object.create(domElement)
    .init('body')
    .appendChild(div)
    .addAttribute('bgcolor', '#012345')
    .addAttribute('id', 'myid');

var root = Object.create(domElement)
    .init('html')
    .appendChild(head)
    .appendChild(body);

console.log(root.innerHTML);
