function generateRandomNumber() {
    //generate random number of array abcd
    let abcd = [];

    for (let i = 0; i < 4; i++) {
        if (i === 0) {
            let leftFirstNumber = Math.floor(Math.random() * 9) + 1;
            abcd.push(leftFirstNumber);
        }
        else {
            let otherNumbers = Math.floor(Math.random() * 10);
            abcd.push(otherNumbers);
        }
    }
    /*console.log(abcd);*/
    return abcd;
}

//count sheep or rams
function counter(count, animal, arrayNum, newNum) {
    for (let j = 0; j < arrayNum.length; j++) {
        if (Number(newNum) === arrayNum[j]) {
            count++;
        }
    }
    if (count > 1) {
        alert(animal + " " + count + "!");
    }
    else if (count === 1) {
        alert(animal + "!");
    }
    else {
        alert("Такой цифры нет!");
    }
}

function chooseCorrectNumber(choosedNumber, xyzw, arrayNumbers) {
    let newNum = prompt("Введите" + " " + choosedNumber + "-e " + "число:");
    if (Number(newNum) === arrayNumbers[choosedNumber - 1]) {
        xyzw[choosedNumber - 1] = newNum;
        return {isCorrect: true, number: newNum};
    }
    return {isCorrect: false, number: newNum};
}

//function to check whether the number from this position is guessed
function isNotNull(choosedNumber, arrayNumbers) {
    if (arrayNumbers[choosedNumber - 1] === null) {
        alert("Цифра с этой позиции отгадана, выберите другую позицию.");
        return false;
    }
    return true;
}


function isInRange(from, to, choosedNumber) {
    if (choosedNumber < from || choosedNumber > to) {
        alert("Выберите цифру в диапозоне от " + from + " до " + to + ".");
        return false;
    }
    return true;
}

//check for emptiness
function isNotEmpty(choosedNumber) {
    if (choosedNumber === "") {
        alert("Введите число.");
        return false;
    }
    return true;
}

//function-validator, if entered uncorrect format
function isCorrectFormat(choosedNumber) {
    if (isNaN(parseInt(choosedNumber))) {
        alert("Введите число.");
        return false;
    }
    return true;
}

function isChoosedNumberValid(choosedNumber, arrayNumbers) {
    return isNotNull(choosedNumber, arrayNumbers) &&
        isNotEmpty(choosedNumber) &&
        isInRange(1, 4, choosedNumber) &&
        isCorrectFormat(choosedNumber);
}

function findCorrectNumber(arrayNumbers) {
    let remainingNumbers = arrayNumbers.length,
        xyzw = [],
        score = 0;
    while (remainingNumbers > 0) {
        let choosedNumber = prompt("Выберите цифру, которую хотите отгадать");
        let k = 0;
        if (!isChoosedNumberValid(choosedNumber, arrayNumbers)) {
            continue;
        }

        let result = chooseCorrectNumber(choosedNumber, xyzw, arrayNumbers);
        if (result.isCorrect) {
            counter(k, "Ram", arrayNumbers, result.number);
            score = score + 10;
            arrayNumbers[choosedNumber - 1] = null;
            remainingNumbers--;
        }
        else {
            counter(k, "Sheep", arrayNumbers, result.number);
            score = score - 5;
        }
    }
    if (remainingNumbers === 0) {
        alert("Поздравляем! Вы угадали все слово");
        let user = prompt("Введите свой ник:");
        outputScore(user, score);
    }
}

//output all scores on page
function outputScore(user, score) {
    localStorage.setItem(user, score);
    document.write("!Scores!" + "</br>" + "</br>");
    for (let i = 0; i < localStorage.length; i++) {
        let keyName = localStorage.key(i);
        let value = localStorage.getItem(keyName);
        document.write(keyName + ": " + value + "</br>");
    }

}

var arrayABCD = generateRandomNumber();
findCorrectNumber(arrayABCD);


