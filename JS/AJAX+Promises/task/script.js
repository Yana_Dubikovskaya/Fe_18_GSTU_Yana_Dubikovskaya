//let listContent = document.getElementsByClassName("block");
function func(file, time, element) {
    document.addEventListener("DOMContentLoaded", function () {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', file, true);
        xhr.onload = setTimeout(function () {
            var ourData = JSON.parse(xhr.responseText);
            console.log(ourData);
            renderHTML(ourData, element);
        }, time);
        xhr.send();
        xhr.onreadystatechange = function () {
            if (this.readyState !== 4) {
                return;
            }
            if (this.status !== 200) {
                alert('ошибка: ' + (this.status ? this.statusText : 'запрос не удался'));
                return;
            }
        }
    });
}

func('first-file.json', 1000, "one-block");
func('second-file.json', 2000, "two-block");
func('third-file.json', 3000, "three-block");
func('fourth-file.json', 4000, "four-block");

function renderHTML(data, element) {
    let listContent = document.getElementById(element);
    let htmlString = document.createElement("p"),
        htmlDate = document.createElement("p");
        htmlDate.className = "date";
    htmlDate.innerHTML = "&#9898; "+data.date;
    htmlString.innerHTML = data.text;
    listContent.appendChild(htmlDate);
    listContent.appendChild(htmlString);
    console.log(listContent);
    htmlString.style.fontSize = "15px";

}
