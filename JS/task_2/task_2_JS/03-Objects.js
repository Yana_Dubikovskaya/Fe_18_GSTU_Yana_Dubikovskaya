console.log("");
console.log("03-Objects");

/*Task 1*/
console.log("Task 1");

function calculateTheDistanceBetweenTwoPoints(p1, p2) {
    let distance = (Math.sqrt(Math.pow(p2.x - p1.x, 2) + (Math.pow(p2.y - p1.y, 2)))).toFixed(2);
    return parseFloat(distance);
}

function isCanBeTriangle(line1, line2, line3) {
    let a = calculateTheDistanceBetweenTwoPoints(line1.pointOne, line1.pointTwo);
    let b = calculateTheDistanceBetweenTwoPoints(line2.pointOne, line2.pointTwo);
    let c = calculateTheDistanceBetweenTwoPoints(line3.pointOne, line3.pointTwo);
    console.log(a);
    console.log(b);
    console.log(c);
    if (a + b > c && a + c > b && b + c > a) {
        console.log("Triangle exists!");
    } else {
        console.log("Triangle doesn't exists");
    }
}

var pointA = {
    x: 5,
    y: 9
};

var pointB = {
    x: 2,
    y: 7
};

var lineA = {
    pointOne: {x: 4, y: 2},
    pointTwo: {x: 1, y: 4}
};

var lineB = {
    pointOne: {x: 2, y: 4},
    pointTwo: {x: 4, y: 3}
};

var lineC = {
    pointOne: {x: 0, y: 0},
    pointTwo: {x: 0, y: 5}
};
var distanceOfPoints = calculateTheDistanceBetweenTwoPoints(pointA, pointB);
console.log("Distance between two points = " + distanceOfPoints);
isCanBeTriangle(lineA, lineB, lineC);
/*End Task 1*/

/*Task 2*/
console.log("Task 2");
Array.prototype.remove = function (element) {
    let arr = [];
    for (let i = 0; i < this.length; i++) {
        if (this[i] !== element) {
            arr.push(this[i]);
        }
    }
    return arr;
}

var sourceArray = [1, 2, 1, 4, 1, 3, 4, 1, 111, 3, 2, 1, "1"];
console.log(sourceArray.remove(1));
/*End Task 2*/

/*Task 3*/
console.log("Task 3");

function getCopy(object) {
    let deep = _.cloneDeep(object); //using library Lodash
    console.log("Copy: ", deep);
    console.log(_.isEqual(deep, object));
}

var sourceObject = {
    a: 1,
    b: {
        c: 'abc'
    }
};
getCopy(sourceObject);
/*End Task 3*/

/*Task 4*/
console.log("Task 4");

function isContainsProperty(object, property) {

    if (object.hasOwnProperty(property)) {
        return true;
    } else {
        return false;
    }
}

var sourceObject = {
    length: 4
};
console.log("Object contains such property: " + isContainsProperty(sourceObject, 'length'));
/*End Task 4*/

/*Task 5*/
console.log("Task 5");

function findsTheYoungestPerson(people) {
    let k = 0;
    let youngestPerson = people[0];
    for (let i = 0; i < people.length; i++) {
        if (youngestPerson.age > people[i].age) {
            youngestPerson = people[i];
            k++;
        }
    }
    return youngestPerson;
}

var persons = [{firstName: "Gosho", lastName: "Petrov", age: 12}, {firstName: "Bay", lastName: "Ivan", age: 81},
    {firstName: "Fedor", lastName: "Ivan", age: 21}
];
var youngest = findsTheYoungestPerson(persons)
console.log(youngest.firstName, youngest.lastName);
/*End Task 5*/

/*Task 6*/
console.log("Task 6");

function group(persons, attribute) {
    return _.groupBy(persons, attribute); //using library Lodash

}

var persons = [{firstName: "Gosho", lastName: "Petrov", age: 12}, {firstName: "Gosho", lastName: "Ivan", age: 81},
    {firstName: "Fedor", lastName: "Ivan", age: 21}
];
console.log(JSON.stringify(group(persons, 'firstName'), null, 2));
/*End Task 6*/