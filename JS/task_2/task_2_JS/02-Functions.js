console.log("");
console.log("02-Functions");
/*Task 1*/
console.log("Task 1");
var returnEnglishWord = function (givenInteger) {
    let lastNumber = givenInteger.toString().slice(-1);

    switch (lastNumber) {
        case '0':
            return "zero";
            break;
        case '1':
            return "one";
            break;
        case '2':
            return "two";
            break;
        case '3':
            return "three";
            break;
        case '4':
            return "four";
            break;
        case '5':
            return "five";
            break;
        case '6':
            return "six";
            break;
        case '7':
            return "seven";
            break;
        case '8':
            return "eight";
            break;
        case '9':
            return "nine";
            break;
        default:
            console.log("error");
    }
}

var givenInteger = 512;
console.log("result: " + returnEnglishWord(givenInteger));
/*End Task 1*/

/*Task 2*/
console.log("Task 2")

function invertNumber(sourceNumber) {
    let invertedNumber = sourceNumber.toString().split("").reverse().join("");
    return parseInt(invertedNumber);
}

var sourceNumber = 512;
console.log(invertNumber(sourceNumber));
/*End Task 2*/

/*Task 3*/
console.log("Task 3")

function printRepeatWordAmount(word, text) {
    let amount = 0;
    let splitedText = text.split(" ");
    for (let i = 0; i < splitedText.length; i++) {
        let tmp = splitedText[i];
        let wordCopy = word;
        // implementing function overloading through built-in arguments 
        if (arguments.length == 2) {
            tmp = tmp.toLowerCase();
            wordCopy = wordCopy.toLowerCase();
        }
        if (wordCopy === tmp) {
            amount++;
        }
    }
    console.log(amount);
}

var sourceWord = "And",
    sourceText = "Here was once a man who travelled the land all over  search a wife. He saw young ANd old, rich and poor, pretty and plain, and could not meet one to his mind.";
printRepeatWordAmount(sourceWord, sourceText);
/*End Task 3*/

/*Task 4*/
console.log("Task 4");

function printAmountDivElements(tag) {
    let tags = document.getElementsByTagName(tag);
    let amount = tags.length;
    console.log("amount of tags: " + amount);
}

var divTag = "div";
printAmountDivElements(divTag);
/*End Task 4*/

/*Task 5*/
console.log("Task 5")

function printAmountOfElementsInArray(array, enteredNumber) {
    let amount = 0;
    for (let i = 0; i < array.length; i++) {
        if (enteredNumber == array[i]) {
            amount++;
        }
    }
    return amount;
}
//test
function test(expected, actual) {
    if (expected == actual) {
        console.log("Test passed!");
    } else {
        console.log("Test failed!")
    }
}

var sourceArray = [4, 5, 7, 7, 9, 1],
    enteredNumber = 7;
var amountOfNumber = printAmountOfElementsInArray(sourceArray, enteredNumber);
console.log("Amount of  ' " + enteredNumber + " ' : " + amountOfNumber);
test(2, amountOfNumber);
/*End Task 5*/

/*Task 6*/
console.log("Task 6")

function getBiggerThanNeighbors(array, index) {
    if (index == 0 || index == array.length - 1) {
        return null;

    } else if (array[index] > array[index - 1] && array[index] > array[index + 1]) {
        return (array[index]);
    } else {
        return null;

    }

}

var sourceArray = [1, 2, 6, 7, 3];
console.log(getBiggerThanNeighbors(sourceArray, 2));
/*End Task 6*/

/*Task 7*/
console.log("Task 7")

function findElement(array) {
    for (let i = 0; i < array.length; i++) {
        //reuse fuction getBiggerThanNeighbors from task 6.
        let result = getBiggerThanNeighbors(array, i);
        if (result != null) {
            return result;
        }
    }
    return -1;
}

var sourceArray = [1, 0, 0, 1, 3];
console.log(findElement(sourceArray));
/*End Task 7*/