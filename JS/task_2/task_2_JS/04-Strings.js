console.log("");
console.log("04-Strings");

/*Task 1*/
console.log("Task 1");

function invertString(string) {
    let invertedString = string.split("").reverse().join("");
    return invertedString;
}

var sourceString = "sample";
console.log(invertString(sourceString));
/*End Task 1*/

/*Task 2*/
console.log("Task 2")

var isBracketsCorrectly = function (expression) {
    expression = expression || '';
    let i,
        count = 0;
    if (expression[0] === ')') {
        return false;
    }
    for (let i = 0; i < expression.length; i++) {
        if (expression[i] === '(') {
            count++;
        }
        if (expression[i] === ')') {
            count--;
        }
    }
    return (count === 0);
};

var expression = '(a+b)';
console.log("Expression", expression, "is", isBracketsCorrectly(expression));
/*End Task 2*/

/*Task 3*/
console.log("Task 3")

function findSubstring(text) {
    let k = 0,
        regexp = /in/gi;
    let matches = text.match(regexp);
    return matches.length;
}

var sourceText = "The text is as follows: We are liv<b>in</b>g **In** an yellow submar<b>in</b>e. We don't have anyth<b>in</b>g else. **In**side the submar<b>in</b>e is very tight. So we are dr<b>in</b>k<b>in</b>g all the day. We will move out of it **in** 5 days.";
console.log("The result: " + findSubstring(sourceText));
/*End Task 3*/

/*Task 4*/
console.log("Task 4")
var text = 'We are <mixcase>living</mixcase> in a <upcase>yellow <lowcase>anything</lowcase> submarine</upcase>.' +
    ' We <mixcase>don\'t</mixcase> have <lowcase>anything</lowcase> else.';
var test = '<upcase>yellow <lowcase>anything</lowcase> submarine</upcase>.';

var stringToHtml = function (str) {
    let template = document.createElement('template');
    template.innerHTML = str;

    return template.content.childNodes;
};

var convertWord = function (word, tagName) {
    let mixcase = 'mixcase'.toUpperCase(),
        lowcase = 'lowcase'.toUpperCase(),
        upcase = 'upcase'.toUpperCase(),
        letters = [],
        i;

    switch (tagName) {
        case mixcase:
            letters = word.split('');
            for (i = 0; i < word.length; i++) {
                if (_.random(0, 1) === 0) {
                    letters[i] = letters[i].toLowerCase();
                } else {
                    letters[i] = letters[i].toUpperCase();
                }
            }
            word = _.join(letters, '');
            break;
        case upcase:
            word = word.toUpperCase();
            break;
        case lowcase:
            word = word.toLowerCase();
            break;
    }

    return word;
};

var beautifyText = function (html) {
    let i,
        j,
        node,
        innerNode,
        result = "",
        allElements = stringToHtml(html);

    console.log(text);
    for (i = 0; i < allElements.length; i++) {
        node = allElements.item(i);
        if (node.childNodes.length > 1) {
            for (j = 0; j < node.childNodes.length; j++) {
                innerNode = node.childNodes.item(j);
                if (innerNode.nodeType === Node.TEXT_NODE) {
                    result += convertWord(innerNode.textContent, node.tagName);
                } else {
                    result += convertWord(innerNode.textContent, innerNode.tagName);
                }
            }
        } else {
            result += convertWord(node.textContent, node.tagName) || '';
        }
    }
    return result;
};

console.log(beautifyText(text));
/*End Task 4*/

/*Task 5*/
console.log("Task 5")

function replace(text) {
    let regexp = /\s/g;
    return text.replace(/\s+/g, "&nbsp");

}

var sourceText = "Carriage quitting securing be appetite it declared.";
console.log(replace(sourceText));
/*End Task 5*/

/*Task 6*/
console.log("Task 6")

function extractText(text) {
    let regexp = /<(?:.|\n)*?>/ig;
    return text.replace(regexp, "");
}

var text = "<html><head><title>Sample site</title></head><body><div>text<div>more text</div>and more...</div>in body </body> </html>";
console.log(extractText(text));
/*End Task 6*/

/*Task 7*/
console.log("Task 7")

function parseURL(url) {
    let element = document.createElement('a');
    element.href = url;

    return {
        protocol: element.protocol,
        server: element.hostname,
        resource: element.pathname
    };
}

var url = "http://www.devbg.org/forum/index.php";
console.log(JSON.stringify(parseURL(url), null, 2));
/*End Task 7*/

/*Task 8*/
console.log("Task 8")

function replaceAllLinks(text) {
    let regexp = /<a href="(.*?)">(.*?)<\/a>/g;
    let replaceTo = '[URL=$1] $2 [/URL]';
    return text.replace(regexp, replaceTo);
}

var text = '<p>Please visit <a href="http://academy.telerik. com">our site</a> to choose a training course. Also visit <a href="www.devbg.org">our forum</a> to discuss the courses.</p>';
console.log(replaceAllLinks(text));
/*End Task 8*/

/*Task 9*/
console.log("Task 9");

function getAllEmails(text) {
    let emailRegexp = /([\w+]+@[\w+]+\.[\w+]+)/gi;
    return text.match(emailRegexp).join('\n');
}

var text = "Hello, world, my name is Yana and my email is: yana1@dubik.com, my second email is: blablabla@eee.ru";
console.log(getAllEmails(text));
/*End Task 9*/

/*Task 10*/
console.log("Task 10");

function findPolindroms(text) {
    let words = text.split(" "),
        polindroms = [];
    for (let i = 0; i < words.length; i++) {
        let invertedWord = invertString(words[i]);
        if (invertedWord.toLowerCase() === words[i].toLowerCase()) {
            polindroms.push(words[i]);
        }
    }
    return polindroms.length > 0 ? polindroms.join('\n') : 'not found';
}

var text = "Exe man, lamal plan, ABBA canal. Panama";
console.log(findPolindroms(text));
/*End Task 10*/

/*Task 11*/
console.log("Task 11");

function stringFormat(text) {
    for (let i = 1; i < arguments.length; i++) {
        let regexp = new RegExp("\\{" + (i - 1) + "\\}", "gi");
        text = text.replace(regexp, arguments[i]);
    }
    return text;
}

var format = "{0}, {1}, {0} text {2}";
var str = stringFormat(format, 1, "Pesho", "Gosho");
console.log(str);
/*Task 11*/

/*Task 12*/
console.log("Task 12");

function clearTemplate(template) {
    return template.substring(1, template.length - 1);
}

function generateList(people, template) {
    let keys = Object.keys(people[0]);
    let list = "<ul>";
    for (let j = 0; j < people.length; j++) {
        list += "<li>"
        let row = template;
        for (let i = 0; i < keys.length; i++) {
            let regexp = new RegExp("-\\{" + keys[i] + "\\}-", "gi");
            let key = keys[i];
            row = row.replace(regexp, people[j][key]);
        }
        list += row + "</li>";
    }
    return list + "</ul>";
}

var people = [{name: "Peter", age: 14}, {name: "Mike", age: 8}, {name: "John", age: 88}, {name: "Max", age: 17}];
var tmpl = document.getElementById("list-item").innerHTML;
console.log(generateList(people, tmpl));
/*End Task 12*/