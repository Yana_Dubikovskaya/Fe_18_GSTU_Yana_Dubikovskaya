console.log("01-Arrays");

/*Task 1*/
console.log("task 1");
var arr = [],
    n = 20;
//initialization array
for (let i = 0; i < n; i++) {
    arr[i] = Math.floor(Math.random() * 10 - 5);
}
//each element multiplied by 5
console.log("Source array: ", arr);
for (let j = 0; j < arr.length; j++) {
    arr[j] = arr[j] * 5;
}
console.log("Received array", arr);
/*End Task 1*/

/*Task 2*/
console.log("task 2");
var charArrayOne = ["sd", "ks", "je"],
    charArrayTwo = ["sd", "ks", "ie"];

//compares 2 arrays
var compareCharArrays = function (arrOne, arrTwo) {
    var flag = false;
    var splittedOne = charArrayOne.join("").split("");
    console.log(splittedOne);
    var splittedTwo = charArrayTwo.join("").split("");
    console.log(splittedTwo);
    if (splittedOne.length != splittedTwo.length) {
        return false;
    } else {
        for (var i = 0; i < splittedOne.length; i++) {

            if (splittedOne[i] === splittedTwo[i]) {
                flag = true;
            } else {
                flag = false;
                break;
            }
        }
    }
    return flag;
};

console.log(compareCharArrays(charArrayOne, charArrayTwo));

/*End Task 2*/

/*Task 3*/
console.log("task 3");
var sourceArray = [2, 1, 1, 2, 3, 3, 2, 2, 2, 1];

var getMaxSequenceOf = function (array, option) {
    array = array || [];
    var repeatCount = 0,
        repeatStartPos,
        repeatEndPos,
        resultArray = [],
        i;
    for (i = 0; i < array.length; i++) {
        if (option(array[i], array[i + 1])) {
            if (repeatCount === 0) {
                repeatStartPos = i;
            }
            repeatCount++;
        } else {
            repeatEndPos = i + 1;
            if (repeatCount >= resultArray.length) {
                resultArray = array.slice(repeatStartPos, repeatEndPos);
            }
            repeatCount = 0;
        }
    }
    return resultArray;
};

console.log("Maximal sequence of equal elements", getMaxSequenceOf(sourceArray, function (a, b) {
    return a === b;
}));
/*End Task 3 */

/**
 *Task 4
 *In this task I reuse my fuction from task 3.
 */
console.log("task 4");
var sourceArray = [3, 2, 3, 4, 2, 2, 4];
console.log("Maximal increasing sequence", getMaxSequenceOf(sourceArray, function (a, b) {
    return a < b;
}));
/*End Task 4*/

/*Task 5*/
console.log("Task 5");
var sourceArray = [5, 7, 1, -6, 0],
    t,
    minIndex;


var sort = function (sourceArray) {
    for (let i = 0; i < sourceArray.length; i++) {
        minIndex = i;
        for (let j = i + 1; j < sourceArray.length; j++) {
            if (sourceArray[j] < sourceArray[minIndex]) {
                minIndex = j;
            }
        }
        t = sourceArray[i];
        sourceArray[i] = sourceArray[minIndex];
        sourceArray[minIndex] = t;
    }
    return sourceArray;
}

console.log("Sorted array: " + sort(sourceArray));
/*End Task 5*/

/*Task 6*/
console.log("Task 6");
var sourceArray = [4, 1, 1, 4, 2, 3, 4, 4, 1, 2, 4, 9, 3];
var max = 0,
    elem = 0,
    tmp = 0;
var getTheMostFrequentNumber = function (sourceArray) {

    for (let i = 0; i < sourceArray.length; i++) {
        for (let j = 0; j < sourceArray.length; j++) {
            if (sourceArray[i] == sourceArray[j]) {
                tmp++;
            }
        }
        if (tmp > max) {
            max = tmp;
            elem = sourceArray[i];
        }
        tmp = 0;
    }
    return elem;
}
console.log("The most frequent number:", getTheMostFrequentNumber(sourceArray), "(", max, "times )");
/*End Task 6*/

/*Task 7*/
console.log("Task 7");

function binarySearch(sourceArray, value) {

    var startIndex = 0,
        stopIndex = 0;

    if (sourceArray.length % 2 == 0) {
        stopIndex = sourceArray.length;
    } else {
        stopIndex = sourceArray.length - 1;
    }

    var middle = Math.floor((stopIndex + startIndex) / 2);
    while (sourceArray[middle] != value && startIndex < stopIndex) {

        if (value < sourceArray[middle]) {
            stopIndex = middle - 1;
        } else if (value > sourceArray[middle]) {
            startIndex = middle + 1;
        }

        middle = Math.floor((stopIndex + startIndex) / 2);
    }

    return (sourceArray[middle] != value) ? -1 : middle;
}

var sourceArray = ["b", "c", "s", "w", "o", "y"];
console.log("Founded index: " + binarySearch(sourceArray, "w"));
/*End Task 7*/