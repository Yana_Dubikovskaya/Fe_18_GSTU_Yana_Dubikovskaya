let example = document.getElementById("home"),
    ctx = example.getContext("2d");
ctx.lineWidth = 3;

ctx.beginPath();
ctx.fillStyle = "#975B5B";
ctx.moveTo(487, 80);
ctx.ellipse(470, 80, 20, 8, Math.PI / 180, 0, 2 * Math.PI);
ctx.moveTo(488, 85);
ctx.lineTo(488, 155);
ctx.lineTo(450, 150);
ctx.lineTo(450, 82);
ctx.fill();
ctx.stroke();

//part of house
ctx.beginPath();
ctx.strokeStyle = "#000";
ctx.fillStyle = "#975B5B";
ctx.fillRect(100, 250, 500, 350);
ctx.strokeRect(100, 250, 500, 350);
ctx.moveTo(100, 250);
ctx.lineTo(350, 30);
ctx.lineTo(600, 250);
ctx.closePath();
ctx.moveTo(450, 200);
ctx.lineTo(450, 120);
ctx.moveTo(487, 150);
ctx.lineTo(487, 200);
ctx.fill();
ctx.fill();
ctx.stroke();


ctx.beginPath();
ctx.lineWidth = 7;
ctx.moveTo(484, 150.5);
ctx.lineTo(454, 118.8);
ctx.strokeStyle = "#975B5B";
ctx.stroke();

//windows
ctx.beginPath();
ctx.fillStyle = "#000";
ctx.fillRect(150, 300, 80, 40);
ctx.fillRect(240, 300, 80, 40);
ctx.fillRect(150, 350, 80, 40);
ctx.fillRect(240, 350, 80, 40);

ctx.fillRect(400, 300, 80, 40);
ctx.fillRect(490, 300, 80, 40);
ctx.fillRect(400, 350, 80, 40);
ctx.fillRect(490, 350, 80, 40);

ctx.fillRect(400, 450, 80, 40);
ctx.fillRect(490, 450, 80, 40);
ctx.fillRect(400, 500, 80, 40);
ctx.fillRect(490, 500, 80, 40);

//door
ctx.beginPath();
ctx.moveTo(170,600);
ctx.lineTo(170, 500);
ctx.bezierCurveTo(185, 435, 280, 445, 290, 505);
ctx.lineTo(292, 600);
ctx.moveTo(231, 600);
ctx.lineTo(231, 455);
ctx.translate(212,550);
ctx.moveTo(5, 0);
ctx.arc(0, 0, 7, 0, Math.PI * 2, false);
ctx.moveTo(40, 0);
ctx.arc(35, 0, 7, 0, Math.PI * 2, false);
ctx.strokeStyle = "#000";
ctx.stroke();


