let example = document.getElementById("bike"),
    ctx = example.getContext("2d");
ctx.lineWidth = 3;

//wheels
ctx.beginPath();
ctx.save();
ctx.translate(200, 300);
ctx.arc(30, 0, 80, 0, Math.PI * 2, false);
ctx.moveTo(420, 0);
ctx.arc(340, 0, 80, 0, Math.PI * 2, false);
ctx.restore();
ctx.strokeStyle = "#398192";
ctx.fillStyle = "#90CAD7";
ctx.fill();
ctx.stroke();

//handlebar
ctx.beginPath();
ctx.moveTo(540, 300);
ctx.lineTo(485,120);
ctx.lineTo(535, 70);
ctx.moveTo(485, 120);
ctx.lineTo(425, 145);
ctx.stroke();

//part of bike
ctx.beginPath();
ctx.moveTo(510, 200);
ctx.lineTo(330, 200);
ctx.lineTo(225, 300);
ctx.lineTo(375, 300);
ctx.lineTo(510, 200);
ctx.stroke();

//bicycle pedals
ctx.beginPath();
ctx.save();
ctx.moveTo(397, 300);
ctx.translate(375,300);
ctx.arc(0, 0, 23, 0, Math.PI * 2, false);
ctx.moveTo(12, 20);
ctx.lineTo(27, 40);
ctx.restore();
ctx.moveTo(360, 285);
ctx.lineTo(340, 260);
ctx.stroke();

//seat
ctx.beginPath();
ctx.moveTo(375, 300);
ctx.lineTo(307, 150);
ctx.lineTo(267, 150);
ctx.moveTo(307, 150);
ctx.lineTo(347, 150);
ctx.stroke();