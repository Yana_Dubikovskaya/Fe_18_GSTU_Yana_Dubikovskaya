let example = document.getElementById("person"),
    ctx = example.getContext("2d");
ctx.lineWidth = 3;

//head
ctx.beginPath();
ctx.arc(300, 300, 90, 0, Math.PI * 2, false);
ctx.strokeStyle = "#FFD178";
ctx.fillStyle = "#FFEBC4";
ctx.fill();
ctx.stroke();

//eyes

//left eye
ctx.beginPath();
ctx.ellipse(260, 280, 15, 10, Math.PI / 180, 0, 2 * Math.PI);
ctx.strokeStyle = "#FFD178";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(259, 280);
ctx.ellipse(256, 280, 5, 10, Math.PI / 180, 0, 2 * Math.PI);
ctx.fillStyle = "#FFD178";
ctx.fill();

//right eye
ctx.beginPath();
ctx.ellipse(330, 280, 15, 10, Math.PI / 180, 0, 2 * Math.PI);
ctx.strokeStyle = "#FFD178";
ctx.stroke();
ctx.beginPath();
ctx.moveTo(325, 280);
ctx.ellipse(326, 280, 5, 10, Math.PI / 180, 0, 2 * Math.PI);
ctx.fill();

//nose
ctx.beginPath();
ctx.moveTo(295, 300);
ctx.lineTo(285, 330);
ctx.lineTo(300, 330);
ctx.stroke();

//mouth
ctx.beginPath();
ctx.moveTo(325, 355);
ctx.rotate(Math.PI/180 * 2);
ctx.ellipse(316, 350, 25, 10, Math.PI / 180, 0, 2 * Math.PI);
ctx.stroke();


//hat
ctx.beginPath();
ctx.moveTo(300, 185);
ctx.ellipse(310, 210, 25, 105, -91 * Math.PI / 180, 0, 2 * Math.PI);
ctx.strokeStyle = "#0E185E";
ctx.fillStyle = "#4D58AB";
ctx.fill();
ctx.stroke();

ctx.beginPath();
ctx.moveTo(265,200);
ctx.lineTo(262, 100);
ctx.bezierCurveTo(272, 112, 352, 112, 365, 100);
ctx.lineTo(368, 200);
ctx.bezierCurveTo(368, 212, 272, 212, 265, 200);
ctx.fill();
ctx.stroke();

ctx.beginPath();
ctx.moveTo(262, 100);
ctx.save();
ctx.translate(314, 100);
ctx.scale(27/9,1);
ctx.arc(0, 0, 16.5, 0, Math.PI * 2, false);
ctx.stroke();
ctx.fill();


